// Exercise 1
class Car {
    name: string;
    acceleration: number = 0;

    constructor(name: string) {
        this.name = name;
    }

    honk() {
        console.log("Tooooooot!");
    }

    accelerate(speed: number): void  {
        this.acceleration += speed;
    } 
}
const car1 = new Car("BMW");
car1.honk();
console.log(car1.acceleration);
car1.accelerate(10);
console.log(car1.acceleration);

// Exercise 2
class BaseObject {
    width: number = 0;
    length: number = 0;
}

class Rectangle extends BaseObject {
    calcSize = (): number => this.width * this.length;
}
const rect = new Rectangle();
rect.width = 6;
rect.length = 2;
console.log(rect.calcSize());

// Exercise 3
class Human {
    private _firstName: string = "";

    get firstName() {
        return this._firstName;
    }

    set firstName(value: string) {
        if (value.length > 3) {
            this._firstName = value;
        } else {
            this._firstName = "";
        }
    }
}

const human = new Human();
console.log(human.firstName);
human.firstName = "Ma";
console.log(human.firstName);
human.firstName = "Reginald";
console.log(human.firstName);
