"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var myName = "Regie";
// string
var hobbies = ["Cooking", "Sports"];
hobbies.push("dilikosure");
console.log(hobbies[2]);
// tuples
var address = ["Superstreet", 99];
// enums
var Color;
(function (Color) {
    Color[Color["Gray"] = 0] = "Gray";
    Color[Color["Green"] = 1] = "Green";
    Color[Color["Blue"] = 2] = "Blue";
})(Color || (Color = {}));
var myColor = Color.Green;
// any
var car = "BMW";
console.log(car);
car = { brand: "BMW", series: 3 };
console.log(car);
// functions
function returnMyName() {
    return myName;
}
console.log(returnMyName());
// void
function sayHello() {
    console.log("Hello!");
}
// argument types
function multiply(value1, value2) {
    return value1 * value2;
}
// console.log(multiply(2, 'Max'));
console.log(multiply(10, 2));
// function types
var myMultiply;
// myMultiply = sayHello;
// myMultiply();
myMultiply = multiply;
console.log(myMultiply(5, 2));
// objects
var userData = {
    name: "Regie",
    age: 26
};
var complex = {
    data: [100, 3.99, 10],
    output: function (all) {
        return this.data;
    }
};
var complex2 = {
    data: [100, 3.99, 10],
    output: function (all) {
        return this.data;
    }
};
//union types
var myRealRealAge = 27;
myRealRealAge = "27";
// myRealRealAge = true;
//check types
var finalValue = 3;
if (typeof finalValue == "number") {
    console.log("Final value is a number and the value is " + finalValue);
}
// never return type
function neverReturns() {
    throw new Error('An error');
}
// Nullable Types
var canBeNull = 12;
canBeNull = null;
var canAlsoBeNull;
canAlsoBeNull = null;
var canThisBeAny = null;
canThisBeAny = 12;
// let & const
console.log("LET & CONST");
var variable = "Test";
console.log(variable);
variable = "Another value";
console.log(variable);
var maxLevels = 100;
console.log(maxLevels);
// maxLevels = 99; //Won't work
// Block scope
function reset() {
    var variable = null;
    console.log(variable);
}
reset();
console.log(variable);
// Arrow Functions
console.log("ARROW FUNCTIONS");
var addNumbers = function (number1, number2) {
    return number1 + number2;
};
console.log(addNumbers(2, 5));
'--------------------------------------------------';
var multiplyNumbers = function (number1, number2) { return (number1 * number2); };
console.log(multiplyNumbers(2, 3));
'--------------------------------------------------';
var greet1 = function () { return console.log("Hello"); };
greet1();
var greetFriend = function (friend) { return console.log(friend); };
greetFriend("Manu");
// Default Parameters
console.log("DEFAULT PARAMETERS");
var countdown = function (start) {
    if (start === void 0) { start = 10; }
    while (start > 0) {
        console.log(start);
        start--;
    }
    console.log("Done!", start);
};
countdown(15);
// Spread
console.log("REST & SPREAD");
var appNumbers = [1, 10, 99, -5];
console.log(Math.max.apply(Math, appNumbers));
// Rest
function makeArray(name) {
    var args1 = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        args1[_i - 1] = arguments[_i];
    }
    return args1;
}
console.log(makeArray("Max", 1, 2, 3, 4, 5));
// Destructuring
console.log("DESTRUCTURING");
var appMyHobbies = ["Cooking", "Sports"];
var hobby1 = appMyHobbies[0], hobby2 = appMyHobbies[1];
console.log(hobby1, hobby2);
var userData1 = { userName: "Max", age: 27 };
var userName = userData1.userName, age = userData1.age;
console.log(userName, age);
// Template Literals
var userName1 = "Max";
var greeting = "Hello, I'm " + userName1 + ". This is cool!";
console.log(greeting);
// classes
var Person = /** @class */ (function () {
    function Person(name, username) {
        this.username = username;
        this.type = "Introvert";
        this.age = 27;
        this.name = name;
    }
    Person.prototype.printAge = function () {
        console.log(this.age);
        this.setType("Old Guy");
    };
    Person.prototype.setType = function (type) {
        this.type = type;
        console.log(this.type);
    };
    return Person;
}());
var person = new Person("Regie", "MrRxbxt");
console.log(person.name, person.username);
person.printAge();
// person.setType("Cool guy");
// Inheritance
var Max = /** @class */ (function (_super) {
    __extends(Max, _super);
    function Max(username) {
        var _this = _super.call(this, "Max", username) || this;
        _this.name = "Max";
        _this.age = 31;
        return _this;
    }
    return Max;
}(Person));
var max = new Max("maximo");
console.log(max);
// Getters & Setters
var Plant = /** @class */ (function () {
    function Plant() {
        this._species = "Default";
    }
    Object.defineProperty(Plant.prototype, "species", {
        get: function () {
            return this._species;
        },
        set: function (value) {
            if (value.length > 3) {
                this._species = value;
            }
            else {
                this._species = "Default";
            }
        },
        enumerable: true,
        configurable: true
    });
    return Plant;
}());
var plant = new Plant();
console.log(plant.species);
plant.species = "AB";
console.log(plant.species);
plant.species = "Green Plant";
console.log(plant.species);
// Static Properties & Methods
var Helpers = /** @class */ (function () {
    function Helpers() {
    }
    Helpers.calcCircumference = function (diameter) {
        return this.PI * diameter;
    };
    Helpers.PI = 3.14;
    return Helpers;
}());
console.log(2 * Helpers.PI);
console.log(Helpers.calcCircumference(8));
// Abstract Classes
var Project = /** @class */ (function () {
    function Project() {
        this.projectName = "Default";
        this.budget = 1000;
    }
    Project.prototype.calcBudget = function () {
        return this.budget * 2;
    };
    return Project;
}());
var ITProject = /** @class */ (function (_super) {
    __extends(ITProject, _super);
    function ITProject() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ITProject.prototype.changeName = function (name) {
        this.projectName = name;
    };
    return ITProject;
}(Project));
var newProject = new ITProject();
console.log(newProject);
newProject.changeName("Super IT Project");
console.log(newProject);
// private constructors
var OnlyOne = /** @class */ (function () {
    function OnlyOne(myName) {
        this.myName = myName;
    }
    OnlyOne.getInstance = function () {
        if (!OnlyOne.instance) {
            OnlyOne.instance = new OnlyOne('The Only One');
        }
        return OnlyOne.instance;
    };
    return OnlyOne;
}());
// let wrong = new OnlyOne('The Only One');
var right = OnlyOne.getInstance();
// console.log(right.name);
