let myName: string = "Regie";

// string
let hobbies: any[] = ["Cooking", "Sports"]
hobbies.push("dilikosure")
console.log(hobbies[2]);

// tuples
let address: [string, number] = ["Superstreet", 99];

// enums
enum Color {
    Gray,
    Green,
    Blue   
}

let myColor: Color = Color.Green;

// any
let car: any  = "BMW";
console.log(car);
car = { brand: "BMW", series: 3}
console.log(car);

// functions
function returnMyName() {
    return myName;
}
console.log(returnMyName());

// void
function sayHello(): void {
    console.log("Hello!");
}

// argument types
function multiply(value1: number, value2: number): number {
    return value1 * value2;
}

// console.log(multiply(2, 'Max'));
console.log(multiply(10,2));

// function types
let myMultiply: (a: number, b: number) => number;
// myMultiply = sayHello;
// myMultiply();
 myMultiply = multiply;
console.log(myMultiply(5, 2));

// objects
let userData: { name: string, age: number } = {
    name: "Regie",
    age: 26
};

let complex: { data: number[], output: (all: boolean) => number[]} = {
    data: [100, 3.99, 10],
    output: function (all: boolean): number[] {
        return this.data;
    }
}

// type alias
type Complex = { data: number[], output: (all: boolean) => number[]};
let complex2: Complex = {
    data: [100, 3.99, 10],
    output: function (all: boolean): number[] {
        return this.data;
    }
}

//union types
let myRealRealAge: number | string = 27;
myRealRealAge = "27";
// myRealRealAge = true;

//check types
let finalValue = 3;
if (typeof finalValue == "number") {
    console.log(`Final value is a number and the value is ${finalValue}`);
}

// never return type
function neverReturns(): never {
    throw new Error('An error');
}

// Nullable Types
let canBeNull: number | null = 12;
canBeNull = null;
let canAlsoBeNull;
canAlsoBeNull = null;
let canThisBeAny = null;
canThisBeAny = 12

// let & const
console.log("LET & CONST");

let variable = "Test";
console.log(variable);
variable = "Another value";
console.log(variable);

const maxLevels = 100;
console.log(maxLevels);
// maxLevels = 99; //Won't work

// Block scope
function reset(){
    let variable = null;
    console.log(variable);
}
reset();
console.log(variable);

// Arrow Functions
console.log("ARROW FUNCTIONS");
const addNumbers = function(number1: number, number2: number): number {
    return number1 + number2;
};
console.log(addNumbers(2, 5));
'--------------------------------------------------'
const multiplyNumbers = (number1: number, number2: number) => (number1 * number2);
console.log(multiplyNumbers(2,3));
'--------------------------------------------------'
const greet1 = () => console.log("Hello");
greet1();

const greetFriend = (friend: string) => console.log(friend);
greetFriend("Manu");


// Default Parameters
console.log("DEFAULT PARAMETERS")
const countdown = (start: number = 10) => {
    while (start > 0) {
        console.log(start);
        start--;
    }
    console.log("Done!", start);
}
countdown(15);

// Spread
console.log("REST & SPREAD");
const appNumbers = [1, 10, 99, -5];
console.log(Math.max(...appNumbers));

// Rest
 function makeArray(name:string,...args1:number[]) {
     return args1;
 }
 console.log(makeArray("Max", 1,2,3,4,5))
 
 // Destructuring
 console.log("DESTRUCTURING");
 const appMyHobbies = ["Cooking", "Sports"];
 const [hobby1, hobby2] = appMyHobbies;
 console.log(hobby1,hobby2);

 const userData1 = {userName: "Max", age: 27};
 const {userName, age} = userData1;
 console.log(userName, age);

// Template Literals
const userName1: string = "Max";
const greeting = `Hello, I'm ${userName1}. This is cool!`;
console.log(greeting);

// classes
class Person {
    public name: string;
    private type: string = "Introvert";
    protected age: number = 27;

    constructor(name: string, public username: string) {
        this.name = name;
    }

    printAge() {
        console.log(this.age);
        this.setType("Old Guy");
    }

    private setType(type:string) {
        this.type = type;
        console.log(this.type)
    }
}

const person = new Person("Regie", "MrRxbxt");
console.log(person.name, person.username);
person.printAge();
// person.setType("Cool guy");

// Inheritance
class Max extends Person {
    name = "Max";

    constructor (username: string){
        super("Max", username);
        this.age = 31;
    }
}
const max = new Max("maximo");
console.log(max);

// Getters & Setters
class Plant {
    private _species: string = "Default";

    get species() {
        return this._species;
    }

    set species(value: string) {
        if (value.length > 3) {
            this._species = value;
        } else {
            this._species = "Default";
        }
    }
}

let plant = new Plant();
console.log(plant.species);
plant.species = "AB";
console.log(plant.species);
plant.species = "Green Plant";
console.log(plant.species);


// Static Properties & Methods
class Helpers {
    static PI: number = 3.14;

    static calcCircumference(diameter: number): number {
        return this.PI * diameter;
    }
}
console.log(2 * Helpers.PI);
console.log(Helpers.calcCircumference(8));

// Abstract Classes
abstract class Project {
    projectName: string = "Default";
    budget: number = 1000;

    abstract changeName(name: string): void;

    calcBudget() {
        return this.budget * 2;
    }
}

class ITProject extends Project {
    changeName(name: string): void {
        this.projectName = name;
    }
}
let newProject = new ITProject();
console.log(newProject);
newProject.changeName("Super IT Project");
console.log(newProject);

// private constructors
class OnlyOne {
    private static instance: OnlyOne;

    private constructor(public readonly myName: string) {
    }

    static getInstance() {
        if (!OnlyOne.instance) {
            OnlyOne.instance = new OnlyOne('The Only One');
        }
        return OnlyOne.instance;
    }
}

// let wrong = new OnlyOne('The Only One');
let right = OnlyOne.getInstance();
// console.log(right.name);